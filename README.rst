Astra-Zeneca Dream Challenge 2015
=================================

Data summary
------------

Drugs

- All: 119
- With synergy scores: 69
- With monotherapy data: 118


Cell lines

- All: 85
- Training data points (a, b, c): 2,199
- Training + test data points (a, b, c): 11,575

